.. dlpoly-py documentation master file, created by
   sphinx-quickstart on Tue Aug 25 17:26:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dlpoly-py's documentation!
=====================================


.. automodule:: dlpoly
   :no-members:

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   about
   DLPoly
   control
   field
   statis
   output
   rdf
   msd
